const { UserHasPromotion } = require('lit-models')
const Base = require('./base')

class UserHasPromotionRepo extends Base {
  constructor(props) {
    super(props)
  }

}

module.exports = new UserHasPromotionRepo(UserHasPromotion)