const httpError = require('http-errors')
const {Op} = require('sequelize')
const {PaymentMethod} = require('lit-models')
const {PAYMENT_METHOD, errorTags} = require('lit-constants')
const Base = require('./base')
const {facade: {DBFacade}} = require('lit-utils')

class PaymentMethodRepo extends Base {
  constructor(model, publicFields) {
    super(model, publicFields)
  }
  findByUserId(userId) {
    return this.model.findAll({where: {userId}})
  }

  findActiveByUserId(userId, options = {}) {
    const conditions = {
      where: {userId, status: PAYMENT_METHOD.STATUS.PRIMARY}
    }
    if (options.transaction) {
      conditions.transaction = options.transaction
    }
    return this.model.findOne(conditions)
  }

  verifyPaymentMethod(pm, u) {
    if (!pm) throw httpError(404, errorTags.MISSING_ACTIVE_PAYMENT_METHOD)
    if (!pm.bankToken) throw httpError(400, errorTags.INVALID_PAYMENT_METHOD)
    if (pm.userId !== u.id) throw httpError(403, errorTags.NOT_OWN_PAYMENT_METHOD)
  }

  async addActive(data) {
    const t = await DBFacade.transaction()
    try {
      data.status = PAYMENT_METHOD.STATUS.PRIMARY
      const pm = await this.create(data, {transaction: t})
      await this.model.update({status: PAYMENT_METHOD.STATUS.SECONDARY}, {
        where: {
          id: {[Op.ne]: pm.id},
          userId: pm.userId
        },
        transaction: t
      })
      await t.commit()
      return pm
    } catch (e) {
      await t.rollback()
      throw e
    }
  }

  isAtm(pm) {
    return pm.type === PAYMENT_METHOD.TYPE.ATM
  }
}


module.exports = new PaymentMethodRepo(PaymentMethod, PAYMENT_METHOD.PUBLIC_DATA)