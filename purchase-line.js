const {PurchaseLine} = require('lit-models')
const {col, fn, Op} = require('sequelize')
const _ = require('lodash')

const Base = require('./base')
const {PURCHASE_CONFIG, PURCHASE_PAYMENT_STATUS} = require('lit-constants')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {QueryTypes} = require('sequelize')


class PurchaseLineRepo extends Base {
  constructor(props) {
    super(props)
  }

  addInstallment(installment, userId, options){
    return this.create({
      label: 'Installment',
      type: PURCHASE_CONFIG.PURCHASE_LINE_TYPE.PURCHASE_INSTALLMENT,
      dueDate: installment.dueDate,
      amount: installment.amount,
      userId: userId,
      operator: PURCHASE_CONFIG.OPERATOR.DUE,
      isRefundable: false,
      purchaseId: installment.purchaseId,
      purchaseInstallmentId: installment.id,
    }, options)
  }

  //Get for each user with balance due
  async searchUserWithBalanceDue(transaction, options = {}) {

    let sqlQuery =
      '  SELECT IFNULL(due.sum_amount, 0) AS totalAmountDue,' +
      '    IFNULL(paid.sum_amount, 0) AS totalAmountPaid,' +
      '    (IFNULL(due.sum_amount, 0) - IFNULL(paid.sum_amount, 0)) AS amountDue,' +
      '    due.user_id as userId ' +
      '    FROM' +
      '    (SELECT SUM(pl.amount) AS sum_amount,' +
      '      pl.user_id' +
      '    FROM purchase_lines  pl ' +
      '    INNER JOIN purchases p ' +
      '    ON pl.purchase_id = p.id' +
      '    AND p.payment_status IN ($inProgressStatus, $installmentFailedStatus)' +
      '    WHERE ' +
      '    OPERATOR = $due' +
      '    GROUP BY pl.user_id) AS due' +
      '    LEFT JOIN' +
      '    (SELECT IFNULL(SUM(pl.amount), 0) AS sum_amount,' +
      '      pl.user_id' +
      '    FROM purchase_lines pl ' +
      '    INNER JOIN purchases p ' +
      '    ON pl.purchase_id = p.id' +
      '    AND p.payment_status IN ($inProgressStatus, $installmentFailedStatus)' +
      '    WHERE ' +
      '    OPERATOR = $paid ' +
      '    GROUP BY pl.user_id) AS paid ON due.user_id = paid.user_id ' +
      '    HAVING totalAmountDue > totalAmountPaid '

    //Filters by userId
    if (_.get(options, 'userId', false))
    {
      sqlQuery += ' AND due.user_id = $userId'
    }


    return await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      sqlQuery,
      {
        bind: {
          due: PURCHASE_CONFIG.OPERATOR.DUE,
          paid: PURCHASE_CONFIG.OPERATOR.PAYMENT,
          inProgressStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
          installmentFailedStatus: PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
          userId: _.get(options, 'userId', null)
        },
        type: QueryTypes.SELECT,
        transaction
      }
    )
  }

  //LINKED WITH function searchUserWithBalanceDue but GROUPED BY purchase id + filter by user id !
  async searchUserWithBalanceDueByPurchaseId(transaction, options = {}) {
    return await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      'SELECT IFNULL(due.sum_amount, 0) AS totalAmountDue,' +
      '    IFNULL(paid.sum_amount, 0) AS totalAmountPaid,' +
      '      (IFNULL(due.sum_amount, 0) - IFNULL(paid.sum_amount, 0)) AS amountDue,' +
      '      due.user_id as userId, due.purchase_id as purchaseId ' +
      '    FROM' +
      '    (SELECT SUM(pl.amount) AS sum_amount,' +
      '      pl.user_id, pl.purchase_id' +
      '    FROM purchase_lines  pl ' +
      '    INNER JOIN purchases p ' +
      '    ON pl.purchase_id = p.id' +
      '    AND p.payment_status IN ($inProgressStatus, $installmentFailedStatus)' +
      '    WHERE' +
      '    OPERATOR = $due' +
      '    AND pl.user_id = $userId ' +
      '    GROUP BY pl.user_id, pl.purchase_id) AS due' +
      '    LEFT JOIN' +
      '    (SELECT IFNULL(SUM(pl.amount), 0) AS sum_amount,' +
      '      pl.user_id, purchase_id' +
      '    FROM purchase_lines pl ' +
      '    INNER JOIN purchases p ' +
      '    ON pl.purchase_id = p.id' +
      '    AND p.payment_status IN ($inProgressStatus, $installmentFailedStatus)' +
      '    WHERE ' +
      '    OPERATOR = $paid ' +
      '    AND pl.user_id = $userId ' +
      '    GROUP BY pl.user_id, pl.purchase_id) AS paid ' +
      '   ON due.user_id = paid.user_id  ' +
      '   AND due.purchase_id = paid.purchase_id ' +
      '   GROUP BY due.purchase_id ' +
      '    HAVING totalAmountDue > totalAmountPaid ',
      {
        bind: {
          due: PURCHASE_CONFIG.OPERATOR.DUE,
          paid: PURCHASE_CONFIG.OPERATOR.PAYMENT,
          inProgressStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
          installmentFailedStatus: PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
          userId: options.userId
        },
        type: QueryTypes.SELECT,
        transaction
      }
    )
  }



  async getDueByPurchaseId(purchaseId) {
    const conditionDue = {
      where: {purchaseId, operator: PURCHASE_CONFIG.OPERATOR.DUE},
      attributes: [[fn('sum', col('amount')), 'sum']]
    }
    const conditionPayment = {
      where: {purchaseId, operator: PURCHASE_CONFIG.OPERATOR.PAYMENT},
      attributes: [[fn('sum', col('amount')), 'sum']]
    }
    const [due, payment] = await Promise.all([
      this.model.findOne(conditionDue),
      this.model.findOne(conditionPayment),
    ])

    return (due.dataValues.sum || 0) - (payment.dataValues.sum || 0)
  }



  async getPaidByUserId(userId, purchaseIds) {
    const condition = {
      attributes: [[fn('sum', col('amount')), 'sum']],
      where: {
        userId: userId
      }
    }

    if (!_.isEmpty(purchaseIds)) condition.where.purchaseId = {[Op.in]: purchaseIds}

    const conditionPayment = _.cloneDeep(condition)
    conditionPayment.where.operator = PURCHASE_CONFIG.OPERATOR.PAYMENT
    const [payment] = await Promise.all([
      this.model.findOne(conditionPayment),
    ])

    return _.get(payment.dataValues, 'sum', 0)
  }


  async getDueByUserId(userId, purchaseIds) {
    const condition = {
      attributes: [[fn('sum', col('amount')), 'sum']],
      where: {
        userId
      }
    }
    if (!_.isEmpty(purchaseIds)) condition.where.purchaseId = {[Op.in]: purchaseIds}
    const conditionDue = _.cloneDeep(condition)
    conditionDue.where.operator = PURCHASE_CONFIG.OPERATOR.DUE
    const conditionPayment = _.cloneDeep(condition)
    conditionPayment.where.operator = PURCHASE_CONFIG.OPERATOR.PAYMENT
    const [due, payment] = await Promise.all([
      this.model.findOne(conditionDue),
      this.model.findOne(conditionPayment),
    ])
    return (due.dataValues.sum || 0) - (payment.dataValues.sum || 0)
  }

  addPayment(purchase, amount, options) {
    return this.create({
      label: 'Installment',
      type: PURCHASE_CONFIG.PURCHASE_LINE_TYPE.PAYMENT,
      amount,
      userId: purchase.userId,
      operator: PURCHASE_CONFIG.OPERATOR.PAYMENT,
      isRefundable: false,
      purchaseId: purchase.id,
    }, options)
  }

  addFirstPayment(purchaseId, userId, amount, options) {
    return this.create({
      label: 'Installment',
      type: PURCHASE_CONFIG.PURCHASE_LINE_TYPE.PAYMENT,
      amount,
      userId: userId,
      operator: PURCHASE_CONFIG.OPERATOR.PAYMENT,
      isRefundable: false,
      purchaseId: purchaseId,
    }, options)
  }
}

module.exports = new PurchaseLineRepo(PurchaseLine)