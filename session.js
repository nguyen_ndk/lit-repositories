const Base = require('./base')
const {Session} = require('lit-models')

class SessionRepo extends Base {
  constructor(props) {
    super(props)
  }

  findByUserId(userId){
      return this.model.findOne({where: {userId: userId}})
  }
}

module.exports = new SessionRepo(Session)