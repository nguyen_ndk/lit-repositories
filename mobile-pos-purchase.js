const {MobilePosPurchase, Merchant, User, PosDevice, PosNotificationChannel} = require('lit-models')
const Base = require('./base')
const {MOBILE_POS_PURCHASE} = require('lit-constants')
class MobilePosPurchaseRepo extends Base {
  constructor(model, publicData) {
    super(model, publicData)
  }

  findByIdWithMerchant(id) {
    const condition = {
      where: {id},
      include: {
        model: Merchant
      }
    }
    return this.model.findOne(condition)
  }

  findByIdWithUser(id) {
    const condition = {
      where: {id},
      include: {
        model: User,
        attributes: ['id', 'email', 'firstName', 'lastName']
      }
    }
    return this.model.findOne(condition)
  }
  complete(id, status, purchaseId, options) {
    const saveData = {
      status,
      purchaseId,
    }
    return this.update(id, saveData, options)
  }

  findByPurchaseId(purchaseId) {
    const condition = {
      where: {purchaseId},
    }
    return this.model.findOne(condition)
  }

  findByPurchaseIdWithNotificationIds(purchaseId) {
    const condition = {
      where: {
        purchaseId
      },
      include: [{
        model: PosDevice,
        include: [{
          model: PosNotificationChannel
        }]
      }]
    }
    return this.model.findOne(condition)
  }
}

module.exports = new MobilePosPurchaseRepo(MobilePosPurchase, MOBILE_POS_PURCHASE.PUBLIC_DATA)