const _ = require('lodash')
const moment = require('moment')
const {QueryTypes, literal, Op, Sequelize} = require('sequelize')

const {Purchase, Merchant, User, PurchaseInstallment, PaymentMethod, MerchantFinancial} = require('lit-models')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {QUERY_METHOD_MAPPING, PURCHASE_PAYMENT_STATUS, PURCHASE, DEFAULT_DATETIME_FORMAT} = require('lit-constants')
const Base = require('./base')

class PurchaseRepo extends Base {
  findById(id, options = {}) {
    const includes = []
    if (options.includeMerchant) {
      includes.push({model: Merchant})
    }
    if (options.includeInstallment) {
      includes.push({model: PurchaseInstallment})
    }
    if (options.includePaymentMethod) {
      includes.push({model: PaymentMethod})
    }
    if (options.includeUser) {
      const includeObj = {model: User}
      if (_.has(options, 'user.attributes')) {
        includeObj.attributes = options.user.attributes
      }
      includes.push(includeObj)
    }
    if (!_.isEmpty(includes)) {
      options.include = includes
    }
    return this.model.findByPk(id, options)
  }

  findByIds(ids) {
    return this.model.findAll({where: {id: {[Op.in]: ids}}})
  }

  findByIdWithMerchant(id, options = {}) {
    // TODO: refactor this function or replace it with findById function
    options.include = [{
      model: Merchant
    }]
    return this.findById(id, options)
  }

  findByMerchantRequestId(merchantRequestId, options = {}) {
    options.where = {merchantRequestId}
    return this.model.findOne(options)
  }

  findByBillIdAndMerchantId(billId, merchantId, options = {}) {
    options.where = {billId, merchantId}
    return this.model.findOne(options)
  }

  async findByMerchantRequestIdForUpdate(merchantRequestId, options = {}) {
    const condition = {
      where: {merchantRequestId},
      transaction: options.transaction,
      lock: true
    }
    return this.model.findOne(condition)
  }

  create(data, options = {}) {
    data.paymentStatus = data.paymentStatus || PURCHASE_PAYMENT_STATUS.INITIALIZED
    data.merchantPaidStatus = data.merchantPaidStatus || PURCHASE.MERCHANT_PAID_STATUS.NOT_YET
    return this.model.create(data, options)
  }

  countHasInstallmentFailedByUserId(userId) {
    const conditions = {
      where: {
        userId,
        paymentStatus: PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED
      }
    }
    return this.model.count(conditions)
  }

  markInProgress(p, transactionId, transaction) {
    return this.update(p.id, {
      paymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
      paidInstallments: 1,
      transactionId,
      payFirstInstallmentAt: moment().format(DEFAULT_DATETIME_FORMAT)
    }, {getModel: false, transaction})
  }

  markFailed(id, userId, transaction) {
    return this.update(id, {
      paymentStatus: PURCHASE_PAYMENT_STATUS.FAILED,
      userId: userId
    }, {getModel: false, transaction})
  }

  refund(p, paymentStatus, transaction) {
    const options = {getModel: false}
    if (transaction) options.transaction = transaction
    return this.update(p.id, {
      paymentStatus,
      refundedAt: moment().format(DEFAULT_DATETIME_FORMAT)
    }, options)
  }

  _buildSyncAccountingQuery(id) {
    const condition = {
      attributes: ['id', 'amount', 'paymentStatus', 'adminFeeAmount', 'userId'],
      where: {
        userId: {
          [QUERY_METHOD_MAPPING.NOT]: null
        },
        paymentStatus: {
          [QUERY_METHOD_MAPPING.NE]: PURCHASE_PAYMENT_STATUS.INITIALIZED
        },
        accountingSyncStatus: PURCHASE.ACCOUNTING_SYNC_STATUS.NEW,
      },
      include: [{
        model: User,
        attributes: ['financeId', 'firstName', 'lastName', 'id'],
        required: true,
        where: {
          financeId: {
            [QUERY_METHOD_MAPPING.NOT]: null
          },
        }
      }, {
        model: PaymentMethod,
        attributes: ['type'],
      }, {
        model: Merchant,
        attributes: ['financeId', 'name', 'id'],
        required: true,
        where: {
          financeId: {
            [QUERY_METHOD_MAPPING.NOT]: null
          },
        },
        include: [{
          model: MerchantFinancial,
          attributes: ['paymentAmount', 'paymentType'],
          required: false,
          where: {
            dateStart: {
              [QUERY_METHOD_MAPPING.LT]: moment().toDate()
            },
            dateEnd: {
              [QUERY_METHOD_MAPPING.GT]: moment().toDate()
            },
          },
          limit: 1,
        }]
      }],
      order: [['id', 'ASC']],
      subQuery: false
    }
    if (id) condition.where = {id}
    return condition
  }

  cursorNotSyncToAccounting() {
    const condition = this._buildSyncAccountingQuery()
    condition.limit = 5
    return this.cursor(condition)
  }

  findByIdForAccounting(id) {
    const condition = this._buildSyncAccountingQuery(id)
    return this.model.findOne(condition)
  }

  async countNotSyncToAccounting() {
    const result = await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      `SELECT COUNT(p.id) count FROM purchases p
          INNER JOIN (
          SELECT COUNT(id) count_finance, merchant_id FROM merchant_financials
          WHERE date_start < $date AND date_end > $date
          GROUP BY merchant_id
          ) finance
          ON finance.merchant_id =  p.merchant_id
          INNER JOIN merchants m
          ON p.merchant_id = m.id
          AND m.finance_id IS NOT NULL
          INNER JOIN users u
          ON p.user_id = u.id
          AND u.finance_id IS NOT NULL
          WHERE p.payment_status <> $paymentStatus
          AND p.user_id IS NOT NULL
          AND p.accounting_sync_status = $accountingSyncStatus`,
      {
        bind: {
          accountingSyncStatus: PURCHASE.ACCOUNTING_SYNC_STATUS.NEW,
          paymentStatus: PURCHASE_PAYMENT_STATUS.INITIALIZED,
          date: moment().format(DEFAULT_DATETIME_FORMAT)
        },
        type: QueryTypes.SELECT,
      }
    )
    return _.get(result, '0.count')
  }

  markSyncAccounting(id) {
    return this.update(id, {accountingSyncStatus: PURCHASE.ACCOUNTING_SYNC_STATUS.DONE})
  }

  _buildUserFilter(conditions) {
    const result = {
      model: User
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }

  _buildMerchantFilter(conditions) {
    const result = {
      model: Merchant
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }

  buildIncludeFilter(conditions) {
    const include = []
    if (conditions.user) {
      include.push(this._buildUserFilter(conditions.user))
    }
    if (conditions.merchant) {
      include.push(this._buildMerchantFilter(conditions.merchant))
    }
    return include
  }

  searchNotSyncToAccounting(page, limit) {
    const condition = {
      page,
      limit,
      attributes: ['amount', 'createdAt'],
      filters: [{
        name: 'userId',
        method: QUERY_METHOD_MAPPING.NOT,
        value: null
      }, {
        name: 'accountingSyncStatus',
        method: QUERY_METHOD_MAPPING.EQ,
        value: PURCHASE.ACCOUNTING_SYNC_STATUS.NEW
      }, {
        name: 'paymentStatus',
        method: QUERY_METHOD_MAPPING.NE,
        value: PURCHASE_PAYMENT_STATUS.INITIALIZED
      }],
      user: {
        attributes: ['email'],
        filters: [{
          name: 'financeId',
          method: QUERY_METHOD_MAPPING.NOT,
          value: null
        }],
      },
      merchant: {
        attributes: ['name'],
        filters: [{
          name: 'financeId',
          method: QUERY_METHOD_MAPPING.NOT,
          value: null
        }],
      }
    }
    return this.search(condition)
  }

  calcPurchaseInstallmentAmount(amount, totalInstallments) {
    return _.ceil(amount / totalInstallments)
  }

  markActive(purchaseId, transaction) {
    const option = {getModel: false}
    if (transaction) option.transaction = transaction
    return this.update(purchaseId, {status: PURCHASE.STATUS.ACTIVE}, option)
  }

  cursorDue(days) {
    const condition = {
      limit: 10,
      where: {
        [literal('CURDATE()')]: literal(`CURDATE() >= DATE(DATE_ADD(pay_first_installment_at, INTERVAL (due_installments * due_gap - ${days} ) DAY))`),
        paymentStatus: {
          [QUERY_METHOD_MAPPING.IN]: [
            PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
            PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
          ]
        },
        [QUERY_METHOD_MAPPING.AND]: [
          Sequelize.literal('paid_installments < total_installments')
        ],
        [QUERY_METHOD_MAPPING.AND]: [
          Sequelize.literal('due_installments < total_installments')
        ]
      },
      attributes: ['id', 'userId', 'amount', 'paidInstallments', 'totalInstallments', 'description', 'dueInstallments'],
      order: [['id', 'ASC']],
      include: [{
        model: User,
        attributes: ['email'],
        // It supposes to be work, but somehow the sql run correctly but the function result is not correct.
        // TODO: let investigate more if we have time, otherwise, let's change the engine after convert our monolith
        //  to micro services.
        // Right now, we will work around by query payment method in a separated query.
        /*include: [{
          model: PaymentMethod,
          attributes: ['type'],
          where: {
            status: PAYMENT_METHOD.STATUS.ACTIVE,
            type: PAYMENT_METHOD.TYPE.ATM,
          }
        }]*/
      }, {
        model: Merchant,
        attributes: ['name']
      }]
    }
    return this.cursor(condition)
  }

  isInstallmentFailed(purchase) {
    return purchase.paymentStatus === PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED
  }

  isCompleted(purchase) {
    return purchase.paymentStatus === PURCHASE_PAYMENT_STATUS.COMPLETED
  }

  increaseDueInstallment(purchase, options) {
    return this.update(purchase.id, {dueInstallments: purchase.dueInstallments + 1}, options)
  }

  payInstallments(purchase, installmentNumber, transaction) {
    const number = installmentNumber > purchase.dueInstallments ? installmentNumber : purchase.dueInstallments
    const saveData = {
      paidInstallments: number,
      dueInstallments: number,
      paymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS
    }
    if (number === purchase.totalInstallments) {
      saveData.paymentStatus = PURCHASE_PAYMENT_STATUS.COMPLETED
      saveData.payLastInstallmentAt = moment().format(DEFAULT_DATETIME_FORMAT)
    }
    const options = {getModel: false}
    if (transaction) options.transaction = transaction
    return this.update(purchase.id, saveData, options)
  }

  payInstallmentsV2(purchase, numberInstallmentPaid) {
    const saveData = {
      paidInstallments: parseInt(purchase.paidInstallments) + parseInt(numberInstallmentPaid)
    }

    if (saveData.paidInstallments === purchase.totalInstallments) {
      saveData.paymentStatus = PURCHASE_PAYMENT_STATUS.COMPLETED
      saveData.payLastInstallmentAt = moment().format(DEFAULT_DATETIME_FORMAT)
    }

    const options = {getModel: false}

    return this.update(purchase.id, saveData, options)
  }

  async countTotalByMerchant(merchantId, transaction) {
    return await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      `SELECT
       COUNT(purchases.id) as total,
       DATE(purchases.created_at) as date
      FROM
       purchases
      WHERE
       purchases.merchant_id=$merchantId
      GROUP BY
       DATE(purchases.created_at)
      `,
      {
        bind: {merchantId},
        type: QueryTypes.SELECT,
        transaction
      }
    )
  }

  async countSumPurchaseByMerchant(merchantId, transaction) {
    return await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      `SELECT
       SUM(purchases.amount) as amount,
       DATE(purchases.created_at) as date
      FROM
       purchases
      WHERE
       purchases.merchant_id=$merchantId
      GROUP BY
       DATE(purchases.created_at)
      `,
      {
        bind: {merchantId},
        type: QueryTypes.SELECT,
        transaction
      }
    )
  }


  //in_progress purchase => installment_failed
  async updateInProgressPurchaseWithFailedInstallment(userId) {
    return await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      `UPDATE purchases AS p
          INNER JOIN purchases_installment AS pi
          ON p.id = pi.purchase_id
          SET payment_status = $newPaymentStatus
          WHERE p.payment_status = $currentPaymentStatus
          AND pi.status IN ($purchaseInstallmentStatus)
          AND p.user_id = $userId`,
      {
        bind: {
          currentPaymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
          newPaymentStatus: PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
          purchaseInstallmentStatus: PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED,
          userId: userId
        },
        type: QueryTypes.UPDATE,
      }
    )
  }

  //installment_failed purchase => in_progress
  async updateFailedInstallmentPurchaseToInprogress(userId) {
    return await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      ` UPDATE purchases AS p
            SET payment_status = $newPaymentStatus
            WHERE p.user_id = $userId
            AND p.payment_status = $currentPaymentStatus
            AND p.id NOT IN (SELECT purchase_id 
                 FROM purchases_installment 
                 WHERE status IN ($purchaseInstallmentStatus)
                 AND user_id = $userId)`,
      {
        bind: {
          currentPaymentStatus: PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
          newPaymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
          purchaseInstallmentStatus: PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED,
          userId: userId
        },
        type: QueryTypes.UPDATE,
      }
    )
  }

  //in_progress purchase => completed IF all installments is paid
  async updateInProgressPurchaseToCompleted(userId) {
    return await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      ` UPDATE purchases AS p
            INNER JOIN (SELECT purchase_id, COUNT(purchase_id), p.total_installments
            FROM purchases_installment as pi
            INNER JOIN purchases as p
            ON p.id = pi.purchase_id
            WHERE pi.status IN ($purchaseInstallmentStatus)
            AND pi.user_id = $userId
            GROUP BY pi.purchase_id
            HAVING COUNT(purchase_id) >= p.total_installments) as pifull
            ON pifull.purchase_id = p.id
            SET payment_status = $newPaymentStatus
            WHERE p.user_id = $userId
            AND p.payment_status = $currentPaymentStatus`,
      {
        bind: {
          currentPaymentStatus: PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
          newPaymentStatus: PURCHASE_PAYMENT_STATUS.COMPLETED,
          purchaseInstallmentStatus: PURCHASE.INSTALLMENT_STATUS.VALIDATED,
          userId: userId
        },
        type: QueryTypes.UPDATE,
      }
    )
  }
}

module.exports = new PurchaseRepo(Purchase, PURCHASE.PUBLIC_DATA)