const {Files} = require('lit-models')
const Base = require('./base')

class FilesRepo extends Base {
  constructor(props) {
    super(props)
  }
}

module.exports = new FilesRepo(Files)