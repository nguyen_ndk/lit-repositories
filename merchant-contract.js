const {MerchantContract} = require('lit-models')
const Base = require('./base')
const {Op} = require('sequelize')

class MerchantContractRepo extends Base {
  constructor(props) {
    super(props)
  }

  findActiveByMerchantId(merchantId) {
    const now = new Date()
    return this.model.findOne({where: {
      merchantId,
      datetimeEnd: {[Op.gte]: now},
      datetimeStart: {[Op.lte]: now},
    }})
  }
}

module.exports = new MerchantContractRepo(MerchantContract)