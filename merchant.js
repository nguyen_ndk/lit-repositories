const _ = require('lodash')
const moment = require('moment')
const httpError = require('http-errors')
const {QueryTypes, Op} = require('sequelize')
const {Merchant, Address, City, Ward, MerchantContact, PosDevice, Category, MarketingCategory,
  District, Company, OnlineStore, Purchase} = require('lit-models')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

const {PURCHASE_PAYMENT_STATUS, DEFAULT_DATETIME_FORMAT, ADDRESS, CITY, WARD, DISTRICT, TRANSACTION_FEE, MERCHANT, errorTags, ADMIN_FEE, INSTALLMENT, QUERY_METHOD_MAPPING} = require('lit-constants')

const Base = require('./base')

class MerchantRepo extends Base {
  constructor(props) {
    super(props)
  }

  findByClientID(clientId, options = {}) {
    if (options.includeAddress) {
      options.include = {
        model: Address,
        attributes: options.slimMode ? ADDRESS.PUBLIC_DATA : null,
        required: false,
        include: [
          {
            model: City,
            attributes: options.slimMode ? CITY.PUBLIC_DATA : null,
          },
          {
            model: District,
            attributes: options.slimMode ? DISTRICT.PUBLIC_DATA : null,
          },
          {
            model: Ward,
            attributes: options.slimMode ? WARD.PUBLIC_DATA : null,
          },
        ],
      }
    }
    
    if (options.includeOnlineStore)
    {
      options.include = {
        model: OnlineStore
      }
    }

    options.where = {clientId}
    options.attributes = options.slimMode ? MERCHANT.PUBLIC_DATA : null
    return Merchant.findOne(options)
  }

  activate(id, options) {
    return this.update(id, {status: MERCHANT.STATUS.ACTIVE}, options)
  }

  findById(id, conditions = {includeOnlineStore: false}) {
    const options = {include: []}
    if (conditions.includeOnlineStore) {
      options.include.push({
        model: OnlineStore
      })
    }
    return super.findById(id, options)
  }

  findWithCompany(id) {
    const condition = {
      where: {id},
      include: [{
        model: Company
      }]
    }
    return this.model.findOne(condition)
  }

  findByIdWithStaffAndPos(id, staffId, posId) {
    const condition = {
      where: {id},
      include: [
        {
          model: PosDevice,
          where: posId ? {id: posId} : null,
          required: !!posId,
        },
      ],
    }
    return this.model.findOne(condition)
  }
  // TODO replace with one in validationService
  verifyMerchantValidity(merchant) {
    if (_.isEmpty(merchant)) throw httpError(404, errorTags.MERCHANT_NOT_FOUND)
    if (merchant.status !== MERCHANT.STATUS.ACTIVE) throw httpError(403, errorTags.MERCHANT_INACTIVE)
  }

  calcInstallment (amount, merchant, total = INSTALLMENT.NUMBER) {
    const result = {
      totalAmount: _.ceil(amount),
      amount: _.ceil(amount / total),
      totalInstallments: total
    }
    switch (merchant.adminFeeType) {
      case ADMIN_FEE.TYPE.AMOUNT:
        result.chargeAmount = result.amount + merchant.adminFeeAmount
        result.totalCharge = result.totalAmount + merchant.adminFeeAmount
        result.adminFeeAmount = merchant.adminFeeAmount
        break
      case ADMIN_FEE.TYPE.PERCENTAGE:
        result.chargeAmount = result.amount + _.round(amount * (merchant.adminFeeAmount / 100))
        result.totalCharge = result.totalAmount + _.round(amount * (merchant.adminFeeAmount / 100))
        result.adminFeeAmount = _.round(merchant.adminFeeAmount * amount / 100)
        break
      default:
        result.chargeAmount = result.amount + TRANSACTION_FEE
        result.totalCharge = result.totalAmount + TRANSACTION_FEE
        result.adminFeeAmount = TRANSACTION_FEE
        break
    }
    return result
  }

  updateAdminFee(id, adminFeeType, adminFeeAmount) {
    if (adminFeeType === ADMIN_FEE.TYPE.PERCENTAGE && adminFeeAmount >= 100) {
      throw httpError(400, errorTags.PASS_100_PERCENTAGE)
    }
    return this.update(id, {
      adminFeeAmount,
      adminFeeType
    })
  }

  cursorNotSyncToAccounting() {
    const condition = {
      limit: 2,
      where: {
        financeId: {
          [QUERY_METHOD_MAPPING.IS]: null
        },
      },
      include: {
        model: MerchantContact,
        required: true,
      },
      order: [['id', 'ASC']]
    }
    return this.cursor(condition)
  }

  updateFinanceId(id, financeId) {
    return this.update(id, {financeId}, {getModel: false})
  }

  _buildCategoryFilter(conditions) {
    const result = {
      model: Category,
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }

  _buildMarketingCategoryFilter(conditions) {
    const result = {
      model: MarketingCategory
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }

  _buildAddressFilter(conditions) {
    const result = {
      model: Address
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }

  _buildMerchantContactFilter(conditions) {
    const result = {
      model: MerchantContact,
      required: conditions.required || false
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }

  buildIncludeFilter(conditions) {
    const include = []
    if (conditions.category) {
      include.push(this._buildCategoryFilter(conditions.category))
    }
    if (conditions.marketingCategory) {
      include.push(this._buildMarketingCategoryFilter(conditions.marketingCategory))
    }
    if (conditions.address) {
      include.push(this._buildAddressFilter(conditions.address))
    }
    if (conditions.merchantContact) {
      include.push(this._buildMerchantContactFilter(conditions.merchantContact))
    }
    return include
  }

  async countNotSyncToAccounting() {
    const result = await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      `SELECT count(id) count from merchants m
      INNER JOIN (
        SELECT merchant_id FROM merchant_contacts
        GROUP by merchant_id
      ) c
      ON c.merchant_id = m.id
      WHERE finance_id is null`,
      {
        type: QueryTypes.SELECT,
      }
    )
    return _.get(result, '0.count')
  }

  searchNotSyncToAccounting(page, limit) {
    const condition = {
      page,
      limit,
      subQuery: false,
      attributes: ['name', 'id'],
      filters: [{
        name: 'financeId',
        method: QUERY_METHOD_MAPPING.IS,
        value: null
      }, {
        name: 'status',
        method: QUERY_METHOD_MAPPING.EQ,
        value: MERCHANT.STATUS.ACTIVE
      }],
      address: {
        attributes: ['fullAddress']
      },
    }
    return this.search(condition)
  }

  isActive(merchant) {
    return merchant.status === MERCHANT.STATUS.ACTIVE
  }

  haveOnlineStore(type) {
    return _.includes([MERCHANT.TYPE.ONLINE_AND_OFFLINE, MERCHANT.TYPE.ONLINE], type)
  }

  getMerchantsPurchases(merchantIds, from, to) {
    const purchaseCondition = {
      userId: {[Op.not]: null},
      paymentStatus: {[Op.ne]: PURCHASE_PAYMENT_STATUS.INITIALIZED}
    }
    const payFirstInstallmentAt = {}
    if (from) {
      payFirstInstallmentAt[Op.gte] = moment(from).startOf('d').format(DEFAULT_DATETIME_FORMAT)
    }
    if (to) {
      payFirstInstallmentAt[Op.lte] = moment(to).endOf('d').format(DEFAULT_DATETIME_FORMAT)
    }
    if (to || from) purchaseCondition.payFirstInstallmentAt = payFirstInstallmentAt
    const condition = {
      where: {
        id: {[Op.in]: merchantIds}
      },
      attributes: ['name'],
      include: [{
        model: Purchase,
        attributes: ['amount', 'originalAmount', 'paymentStatus', 'id', 'merchantRequestId', 'merchantOrderId', 'payFirstInstallmentAt'],
        where: purchaseCondition
      }]
    }
    return this.model.findAll(condition)
  }
}

module.exports = new MerchantRepo(Merchant)
