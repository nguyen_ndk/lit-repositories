const {MerchantCategory} = require('lit-models')
const Base = require('./base')

class MerchantCategoryRepo extends Base {
  constructor(props) {
    super(props)
  }
}

module.exports = new MerchantCategoryRepo(MerchantCategory)