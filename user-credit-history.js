const _ = require('lodash')
const {UserCreditHistory} = require('lit-models')

const create = async (data, options) => {
  const saveData = _.pick(data, ['userId', 'amount', 'purchaseId', 'type'])
  return await UserCreditHistory.create(saveData, options)
}
module.exports = {
  create,
}