const {PromotionHasMerchant} = require('lit-models')
const Base = require('./base')
const _ = require('lodash')

class PromotionMerchantRepo extends Base {
  constructor(props) {
    super(props)
  }


  /*
  Create Promotion Company from list of CompaniesList
   */
  createByMerchantsIdList(promotionId, merchantsList)
  {
    var promotionMerchantArray = []
    _.forEach(merchantsList, merchantId => {
      var data = {
        promotionId: promotionId,
        merchantId: merchantId
      }
      promotionMerchantArray.push(this.create(data))
    })


    return promotionMerchantArray
  }


}

module.exports = new PromotionMerchantRepo(PromotionHasMerchant)