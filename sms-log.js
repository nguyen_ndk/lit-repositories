const Base = require('./base')
const {SmsLog} = require('lit-models')

class SmsLogRepo extends Base {
  constructor(props) {
    super(props)
  }
}

module.exports = new SmsLogRepo(SmsLog)