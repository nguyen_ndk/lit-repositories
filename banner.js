const { Banner } = require('lit-models')
const Base = require('./base')

class BannerRepo extends Base {
  constructor(props) {
    super(props)
  }

  async findAllByPosition(position) {
    return this.model.findAndCountAll({ where: { position } })
  }
}

module.exports = new BannerRepo(Banner)
