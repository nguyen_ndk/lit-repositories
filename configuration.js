const {Configuration} = require('lit-models')
const Base = require('./base')

class ConfigurationRepo extends Base {
  constructor(props) {
    super(props)
  }

  upsert(data) {
    const condition = {name: data.name}
    return super.upsert(data, condition)
  }

  findByName(name) {
    return this.model.findOne({where: {name}})
  }

  updateAccountingAuth(key, value) {
    const conditions = {
      where: {name: key}
    }
    return this.model.update({value}, conditions)
  }
  getAccountingAuth(key) {
    const conditions = {
      where: {name: key}
    }
    return this.model.findOne(conditions)
  }
}
module.exports = new ConfigurationRepo(Configuration)