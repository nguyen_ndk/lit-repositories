const {KycRequestCounting} = require('lit-models')
const Base = require('./base')
class KycRequestCountingRepo extends Base {
  constructor(model) {
    super(model)
  }
}
module.exports = new KycRequestCountingRepo(KycRequestCounting)