const {MerchantFinancial} = require('lit-models')
const Base = require('./base')

class MerchantFinancialRepo extends Base {
  constructor(props) {
    super(props)
  }
}

module.exports = new MerchantFinancialRepo(MerchantFinancial)