const _ = require('lodash')
const {Category, Merchant} = require('lit-models')
const Base = require('./base')

const {CATEGORY} = require('lit-constants')

class CategoryRepo extends Base {
  _buildMerchantFilter(conditions) {
    const result = {
      model: Merchant
    }
    if (!_.isUndefined(conditions.attributes)) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    if (conditions.required) result.required = conditions.required

    return result
  }

  buildIncludeFilter(conditions) {
    const include = []

    if (conditions.includeMerchant) {
      include.push(this._buildMerchantFilter(conditions.includeMerchant))
    }
    return include
  }



}



module.exports = new CategoryRepo(Category, CATEGORY.PUBLIC_DATA)