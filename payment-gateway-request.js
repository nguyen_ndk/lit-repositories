const {PaymentMethod, PaymentGatewayRequest, Purchase} = require('lit-models')
const Base = require('./base')
const {PAYMENT_GATEWAY_REQUEST} = require('lit-constants')

class PaymentGatewayRequestRepo extends Base {
  constructor(model, publicFields) {
    super(model, publicFields)
  }
  findByTransactionId(transactionId){
    return this.model.findOne({where: {transactionId}})
  }

  cursorInitRequest() {
    const condition = {
      where: {
        status: PAYMENT_GATEWAY_REQUEST.STATUS.INIT
      },
      limit: 10
    }
    return this.cursor(condition)
  }
  findByTranIdWithPurchase(transactionId) {
    const condition = {
      where: {transactionId},
      attributes: ['id'],
      include: [{
        model: Purchase
      }]
    }
    return this.model.findAll(condition)
  }
  findByTranIdWithPaymentMethod(transactionId) {
    const condition = {
      where: {transactionId},
      attributes: ['id'],
      include: [{
        model: PaymentMethod
      }]
    }
    return this.model.findOne(condition)
  }

  findAddCardByUserId(userId) {
    const condition = {
      where: {
        userId,
        type: PAYMENT_GATEWAY_REQUEST.TYPE.ADD_PAYMENT_METHOD,
        status: PAYMENT_GATEWAY_REQUEST.STATUS.PROCESSED
      },
    }
    return this.model.findAll(condition)
  }
}


module.exports = new PaymentGatewayRequestRepo(PaymentGatewayRequest)