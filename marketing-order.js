const {MarketingOrder} = require('lit-models')
const Base = require('./base')
class MarketingOrderRepo extends Base {
  constructor(props) {
    super(props)
  }
}
module.exports = new MarketingOrderRepo(MarketingOrder)