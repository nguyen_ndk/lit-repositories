const { LitUser } = require('lit-models')
const Base = require('./base')

class LitUserRepo extends Base {
  constructor(props) {
    super(props)
  }

  findByEmail(email) {
    return LitUser.findOne({ where: { email } })
  }
}

module.exports = new LitUserRepo(LitUser)
