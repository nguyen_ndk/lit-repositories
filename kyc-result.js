const {KycResult, KalapaResponse} = require('lit-models')
const {KYC} = require('lit-constants')

const Base = require('./base')

class KycResultRepo extends Base {
  countByUser(userId) {
    const condition = {where: {userId}}
    return this.model.count(condition)
  }

  findByUserIdWithResponse(userId, getLast) {
    const condition = {
      where: {userId},
      include: [{
        model: KalapaResponse
      }]
    }
    if (getLast) {
      condition.limit = 1
      condition.order = [['id', 'DESC']]
    }
    return this.model.findAll(condition)
  }
  findLatestByUserId(userId) {
    const condition = {
      limit: 1,
      where: {userId},
      order: [['id', 'DESC']]
    }
    return this.model.findOne(condition)
  }

  findLatestPassedByUserId(userId) {
    const condition = {
      limit: 1,
      where: {
        userId,
        phraseTwoStatus: KYC.STATUS.PASSED
      },
      order: [['id', 'DESC']]
    }
    return this.model.findOne(condition)
  }
}

module.exports = new KycResultRepo(KycResult)