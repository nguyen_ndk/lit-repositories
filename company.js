const { Company } = require('lit-models')

const findById = async (id) => {
  return await Company.findOne({ where: { id } })
}

const findAll = async () => {
  return await Company.findAll()
}

const findAndCountAll = async (options) => {
  return await Company.findAndCountAll(options)
}

const create = async (data, options = {}) => {
  return await Company.create(data, options)
}

const destroy = async (id) => {
  return await Company.destroy({ where: { id } })
}

const update = async (id, data, options = { getModel: true }) => {
  const conditions = {
    where: { id },
  }
  if (options.transaction) {
    conditions.transaction = options.transaction
  }

  const result = await Company.update(data, conditions)
  if (result[0] > 0 && options.getModel) {
    return await findById(id)
  }
  return result[0] > 0
}

module.exports = {
  findById,
  findAndCountAll,
  create,
  update,
  destroy,
  findAll,
}
