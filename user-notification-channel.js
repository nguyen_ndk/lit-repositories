const { UserNotificationChannel } = require('lit-models')
const Base = require('./base')

class UserNotificationChannelRepo extends Base {
  findByIdentifier(identifier) {
    return this.model.findOne({where: {identifier}})
  }
}

module.exports = new UserNotificationChannelRepo(UserNotificationChannel)
