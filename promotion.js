const {Promotion, Files, UserHasPromotion} = require('lit-models')
const sequelize = require('sequelize')
const Base = require('./base')
const _ = require('lodash')

const { errorTags, PROMOTION } = require('lit-constants')
class PromotionRepo extends Base {
  constructor(props) {
    super(props)
  }

  findByCode(code) {
    code = code.toUpperCase()
    return Promotion.findOne({where: {code}})
  }

  findAllUsByCode(code){
    if (!code){
      return null
    }
    code = code.toUpperCase()
    return Promotion.findOne({
      where: {
        code: code,
      },
      include: {
        required: false,
        model: UserHasPromotion,
        where: {status: true},
        attributes: [
          [sequelize.fn('sum', sequelize.col('discount_amount')), 'total_amount'],
        ],
      }
    })
  }
  // 

  async verifyPurchase(purchaseInfo, voucherCode){
    let isApprove = true
    let discountAmount = 0
    let currentTime = new Date()
    let promotion = await this.findAllUsByCode(voucherCode)
    if (!promotion){
      isApprove = false
      return {
        discountAmount: discountAmount,
        promotionId: 0,
        amountCharge:  purchaseInfo['amount'] - discountAmount, //TODO: rename to endAmount
        isApprove: isApprove,
        originAmount: purchaseInfo['amount'],
        message: errorTags.PROMOTION_NOT_FOUND,
        currentDiscount: 0,
        promotion: null
      }
    }

    if (purchaseInfo.amount < promotion['minPurchaseAmount']){
      isApprove = false
      return {
        discountAmount: discountAmount,
        promotionId: 0,
        amountCharge:  purchaseInfo['amount'] - discountAmount,
        isApprove: isApprove,
        originAmount: purchaseInfo['amount'],
        message: errorTags.NOT_ENOUGH_MIN_PURCHASE_AMOUNT,
        currentDiscount: 0,
        promotion: promotion
      }
    }
    if (currentTime < promotion['datetimeStart']){
      isApprove = false
      return {
        discountAmount: discountAmount,
        promotionId: 0,
        amountCharge:  purchaseInfo['amount'] - discountAmount,
        isApprove: isApprove,
        originAmount: purchaseInfo['amount'],
        message: errorTags.PROMOTION_NOT_AVAILABLE_IN_TIME,
        currentDiscount: 0,
        promotion: promotion
      }
    }
    if (currentTime > promotion['datetimeEnd']){
      isApprove = false
      return {
        discountAmount: discountAmount,
        promotionId: 0,
        amountCharge:  purchaseInfo['amount'] - discountAmount,
        isApprove: isApprove,
        originAmount: purchaseInfo['amount'],
        message: errorTags.PROMOTION_EXPIRED,
        currentDiscount: 0,
        promotion: promotion
      }
    }
    let userPromotion = await UserHasPromotion.findOne(
      {
        where: {
          promotionId: promotion.id,
          userId: purchaseInfo.userId
        }
      }
    )
    if (!promotion.isCheckout){
      if (!userPromotion || userPromotion.status || userPromotion.amountVoucherUsed == userPromotion.amountVoucher){
        isApprove = false
        return {
          discountAmount: discountAmount,
          promotionId: 0,
          amountCharge:  purchaseInfo['amount'] - discountAmount,
          isApprove: isApprove,
          originAmount: purchaseInfo['amount'],
          message: errorTags.PROMOTION_NOT_APPLY,
          currentDiscount: userPromotion ? userPromotion.discountAmount : 0,
          promotion: promotion
        }
      }
    } else {
      if ((userPromotion && userPromotion.status) || promotion.maxVoucherCanClaim == 0){
        isApprove = false
        return {
          discountAmount: discountAmount,
          promotionId: 0,
          amountCharge:  purchaseInfo['amount'] - discountAmount,
          isApprove: isApprove,
          originAmount: purchaseInfo['amount'],
          message: errorTags.PROMOTION_NOT_APPLY,
          currentDiscount: userPromotion ? userPromotion.discountAmount : 0,
          promotion: promotion
        }
      }
    }

    if (promotion['amountType'] === PROMOTION.TYPE.PERCENTAGE){
      discountAmount = promotion['amount'] * purchaseInfo['amount'] / 100
      if (discountAmount > promotion['maxFlatAmount']){
        discountAmount =  promotion['maxFlatAmount']
      }
    }

    if (promotion['amountType'] === PROMOTION.TYPE.AMOUNT){
      discountAmount = promotion['amount']
    }

    if (promotion['UserHasPromotions'].length > 0){
      let totalDiscount = promotion['UserHasPromotions'][0]
      if (parseInt(totalDiscount['total_amount']) + discountAmount > promotion['budgetMax']){
        discountAmount = 0
        isApprove = false
        return {
          discountAmount: discountAmount,
          promotionId: promotion.id,
          amountCharge:  purchaseInfo['amount'] - discountAmount,
          isApprove: isApprove,
          originAmount: purchaseInfo['amount'],
          message: errorTags.PROMOTION_NOT_AVAILABLE,
          currentDiscount:userPromotion ? userPromotion.discountAmount : 0,
          promotion: promotion
        }
      }
    }
    return {
      discountAmount: discountAmount,
      promotionId: promotion.id,
      amountCharge:  purchaseInfo['amount'] - discountAmount,
      isApprove: isApprove,
      originAmount: purchaseInfo['amount'],
      message: PROMOTION.STATUS.SUCCESS,
      currentDiscount:userPromotion ? userPromotion.discountAmount : 0,
      promotion: promotion
    }
  }
  _buildFileIconFilter(conditions) {
    const result = {
      model: Files,
      as: 'FileIcon'
    }

    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)

    return result
  }

  _buildFileBannerFilter(conditions) {
    const result = {
      model: Files,
      as: 'FileBanner'
    }

    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)

    return result
  }

  _buildFileThumbnailFilter(conditions) {
    const result = {
      model: Files,
      as: 'FileThumbnail'
    }

    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)

    return result
  }

  buildIncludeFilter(conditions) {
    const include = []
    if (conditions.includeFileIcon) {
      include.push(this._buildFileIconFilter(conditions.includeFileIcon))
    }

    if (conditions.includeFileBanner) {
      include.push(this._buildFileBannerFilter(conditions.includeFileBanner))
    }

    if (conditions.includeFileThumbnail) {
      include.push(this._buildFileThumbnailFilter(conditions.includeFileThumbnail))
    }

    if (conditions.includeUserHasPromotion){
      include.push({
        required: false,
        model : UserHasPromotion,
        where: conditions['userQuery']
      })
    }
    return include
  }

}

module.exports = new PromotionRepo(Promotion)