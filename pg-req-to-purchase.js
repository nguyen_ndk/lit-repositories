const {PGReqToPurchase} = require('lit-models')
const Base = require('./base')

class PgReqToPurchaseRepo extends Base {
}

module.exports = new PgReqToPurchaseRepo(PGReqToPurchase)