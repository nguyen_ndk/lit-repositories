module.exports = {
  UserRepo: require('./user'),
  KycReqCountingRepo: require('./kyc-request-counting'),
  MerchantRepo: require('./merchant'),
  PaymentMethodRepo: require('./payment-method'),
  PurchaseRepo: require('./purchase'),
  PurchaseInstallmentRepo: require('./purchase-installment'),
  TranslationRepo: require('./translation'),
  UserCreditHistoryRepo: require('./user-credit-history'),
  LitUserRepo: require('./lit-user'),
  CategoryRepo: require('./category'),
  PincodeRepo: require('./pincode'),
  PaymentGatewayRequestRepo: require('./payment-gateway-request'),
  PgReqToPurchaseRepo: require('./pg-req-to-purchase'),
  CompanyRepo: require('./company'),
  MarketingRepo: require('./marketing'),
  StaffRepo: require('./staff'),
  MerchantStaffRepo: require('./merchant-staff'),
  MobilePosPurchaseRepo: require('./mobile-pos-purchase'),
  MerchantContactRepo: require('./merchant-contact'),
  MerchantContractRepo: require('./merchant-contract'),
  MerchantCategoryRepo: require('./merchant-category'),
  MerchantFinancialRepo: require('./merchant-financial'),
  PosDeviceRepo: require('./pos-device'),
  ConfigurationRepo: require('./configuration'),
  MarketingCategoryRepo: require('./marketing-category'),
  PurchaseAccountingRepo: require('./purchase-accounting'),
  SmsLogRepo: require('./sms-log'),
  BannerRepo: require('./banner'),
  KalapaResponseRepo: require('./kalapa-response'),
  LateFeeRepo: require('./late-fee'),
  PurchaseLineRepo : require('./purchase-line'),
  KycResultRepo : require('./kyc-result'),
  UserCompletionRepo : require('./user-completion'),
  PromotionRepo: require('./promotion'),
  PromotionCompanyRepo: require('./promotion-has-company'),
  PromotionMerchantRepo: require('./promotion-has-merchant'),
  PromotionCategoryRepo: require('./promotion-has-category'),
  FilesRepo: require('./files'),
  OnlineStoreRepo: require('./online-store'),
  MarketingOrderRepo: require('./marketing-order'),
  MarketingHasCategory: require('./marketing-has-category'),
  UserHasPromotionRepo: require('./user-has-promotion'),
  PosNotificationChannelRepo: require('./pos-notification-channel'),
  UserNotificationChannelRepo: require('./user-notification-channel'),
  AppsFlyerRepo: require('./apps-flyer'),
  SessionRepo: require('./session')
}