const _ = require('lodash')
const {Translation} = require('lit-models')

const findAll = async () => {
  return await Translation.findAll()
}
const findByTag = async (tag) => {
  return await Translation.findOne({where: {tag: tag}})
}
const create = async (data) => {
  const saveData = _.pick(data, ['tag', 'en', 'vn'])
  return await Translation.create(saveData)
}

const update = async (id, data) => {
  return await Translation.update(data, {
    where: {id}
  })
}

const destroy = async (id) => {
  return await Translation.destroy({where: {id}})
}

module.exports = {
  create,
  update,
  findByTag,
  findAll,
  destroy
}