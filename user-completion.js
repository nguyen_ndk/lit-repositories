const {UserCompletion} = require('lit-models')
const Base = require('./base')


class UserCompletionRepo extends Base {
  findByUserId(userId, transaction, lock) {
    const condition = {
      where: {
        userId
      },
      transaction,
      lock
    }
    return this.model.findOne(condition)
  }
}

module.exports = new UserCompletionRepo(UserCompletion)