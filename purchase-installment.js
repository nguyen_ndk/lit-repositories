const _ = require('lodash')

const {PurchaseInstallment, Purchase} = require('lit-models')
const moment = require('moment')
const {DEFAULT_DATETIME_FORMAT, PURCHASE} = require('lit-constants')
const Base = require('./base')

class PurchaseInstallmentRepo extends Base {
  constructor(model, publicFields) {
    super(model, publicFields)
  }

  findById(id, options = {}) {
    const includes = []
    if (options.includePurchase) {
      includes.push({model: Purchase})
    }
    if (!_.isEmpty(includes)) {
      options.include = includes
    }
    return this.model.findByPk(id, options)
  }

  findByPurchaseId(purchaseId) {
    const conditions = {
      where: {purchaseId}
    }
    return this.findAll(conditions)
  }

  buildInstallmentData(p, amount, pmId, installmentNumber, transactionId) {
    const result = []
    const paidAt = moment().format(DEFAULT_DATETIME_FORMAT)
    for (let i = p.dueInstallments + 1; i<= installmentNumber; i++) {
      result.push({
        purchaseId: p.id,
        userId: p.userId,
        amount,
        installmentNumber: i,
        paymentMethodId: pmId,
        transactionId,
        status: PURCHASE.INSTALLMENT_STATUS.VALIDATED,
        paidAt
      })
    }

    return result
  }

  addSuccessInstallment(p, amount, pmId, transactionId, transaction) {
    const paidAt = moment().format(DEFAULT_DATETIME_FORMAT)
    const data = {
      purchaseId: p.id,
      userId: p.userId,
      amount,
      installmentNumber: p.paidInstallments + 1,
      paymentMethodId: pmId,
      transactionId,
      status: PURCHASE.INSTALLMENT_STATUS.VALIDATED,
      paidAt
    }

    return this.create(data, {transaction})
  }

  refund(id, tranId) {
    return this.update(id, {
      status: PURCHASE.INSTALLMENT_STATUS.REFUNDED,
      transactionId: tranId,
      refundedAt: moment().format(DEFAULT_DATETIME_FORMAT)
    }, {getModel: false})
  }
  // TODO move this to base repo
  updateMany(data, options) {
    return this.model.update(data, options)
  }


  _buildPurchaseFilter(conditions) {
    const result = {
      model: Purchase,
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }

  buildIncludeFilter(conditions) {
    const include = []
    if (conditions.purchase) {
      include.push(this._buildPurchaseFilter(conditions.purchase))
    }
    return include
  }

  addDue(amount, purchaseId, installmentNumber, userId, options) {
    const saveData = {
      purchaseId,
      userId,
      amount,
      installmentNumber,
      status: PURCHASE.INSTALLMENT_STATUS.DUE,
      dueDate: moment().format(DEFAULT_DATETIME_FORMAT)
    }
    return this.create(saveData, options)
  }
}

module.exports = new PurchaseInstallmentRepo(PurchaseInstallment, PURCHASE.PUBLIC_DATA)