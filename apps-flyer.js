const { AppsFlyer } = require('lit-models')
const Base = require('./base')

class AppsFlyerRepo extends Base {
}

module.exports = new AppsFlyerRepo(AppsFlyer)
