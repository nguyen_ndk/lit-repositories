const {Marketing} = require('lit-models')


const findByMerchantId = async (merchantId, options = {}) => {
  options.where = {merchantId}
  return await Marketing.findOne(options)
}


const findById = async id => {
  return await Marketing.findOne({where: {id}})
}
const findAndCountAll = async options => {
  return await Marketing.findAndCountAll(options)
}

const create = async (data, options = {}) => {
  return await Marketing.create(data, options)
}

const destroy = async (id) => {
  return await Marketing.destroy({where: {id}})
}

const update = async (id, data, options = {getModel: true}) => {
  const conditions = {
    where: {id}
  }
  if (options.transaction) {
    conditions.transaction = options.transaction
  }

  const result = await Marketing.update(data, conditions)

  if (result[0] > 0 && options.getModel) {
    return await findById(id)
  }
  return result[0] > 0
}

module.exports = {
  findByMerchantId,
  findById,
  findAndCountAll,
  create,
  update,
  destroy,
}