const {MERCHANT_STAFF} = require('lit-constants')
const {Staff, Merchant} = require('lit-models')
const {Op} = require('sequelize')

const Base = require('./base')

class StaffRepo extends Base {
  constructor(props) {
    super(props)
  }

  create(data) {
    const saveData = data
    if (!saveData.status) saveData.status = MERCHANT_STAFF.STATUS.ACTIVE
    return Staff.create(saveData)
  }

  findByEmail(email) {
    return Staff.findOne({where: {email}})
  }

  findMerchantsById(id, merchantIds) {
    const merchantCondition = {}
    if (merchantIds) {
      merchantCondition['id'] = {[Op.in]: merchantIds}
    }
    const condition = {
      where: {
        id
      },
      attributes: [],
      include: [{
        model: Merchant,
        where: merchantCondition,
      }]
    }
    return this.model.findOne(condition)
  }
}

module.exports = new StaffRepo(Staff)