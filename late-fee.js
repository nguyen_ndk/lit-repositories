const {LateFee} = require('lit-models')
const Base = require('./base')

class LateFeeRepo extends Base {
  constructor(props) {
    super(props)
  }
}

module.exports = new LateFeeRepo(LateFee)