const _ = require('lodash')
const {DEFAULT_PAGE_LENGTH, QUERY_METHOD_MAPPING} = require('lit-constants')
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils')
const dbConnection = DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection()

class Cursor {
  constructor(model, condition) {
    this.model = model
    this.condition = condition
    if (!this.condition.limit) this.condition.limit = DEFAULT_PAGE_LENGTH
    if (!this.condition.offset) this.condition.offset = 0
    this.noMore = false
  }
  async nextOffSet() {
    if (this.noMore) return []
    const list = await this.model.findAll(this.condition)
    this.condition.offset = this.condition.offset + this.condition.limit
    if (list.length === 0) {
      this.noMore = true
    }
    return list
  }

  async next() {
    if (this.noMore) return []
    const list = await this.model.findAll(this.condition)
    if (list.length === 0) {
      this.noMore = true
    }
    return list
  }
}

class Base {
  constructor(model, publicFields = []) {
    this.model = model
    this.publicFields = publicFields
  }

  findById(id, options = {}) {
    options.where = {id}
    return this.model.findOne(options)
  }

  findOne(options) {
    return this.model.findOne(options)
  }

  findAndCountAll(options) {
    return this.model.findAndCountAll(options)
  }

  findAll(options) {
    return this.model.findAll(options)
  }

  create(data, options = {}) {
    return this.model.create(data, options)
  }

  destroy(id) {
    return this.model.destroy({where: {id}})
  }

  destroyFilter(filters)
  {
    return this.model.destroy(filters)
  }

  /**
   *
   * @param id integer
   * @param data object
   * @param options object
   * @param options.getModel boolean
   * @param options.transaction object
   * @returns {Promise<*|boolean>}
   */
  async update(id, data, options = {getModel: true}) {
    const conditions = {
      where: {id}
    }
    if (options.transaction) {
      conditions.transaction = options.transaction
    }

    const result = await this.model.update(data, conditions)
    if (result[0] > 0 && options.getModel) {
      return await this.findById(id)
    }
    return result[0] > 0
  }

  getPublicData(instance) {
    if (_.isEmpty(this.publicFields)) return instance
    return _.pick(instance, this.publicFields)
  }

  bulkCreate(data, options){
    return this.model.bulkCreate(data, options)
  }

  upsert(data, condition) {
    return this.model
      .findOne({ where: condition })
      .then((obj) => {
        if (obj)
          return obj.update(data)
        return this.model.create(data)
      })
  }

  async stream(condition, onData, onEnd) {
    if (!condition.limit) condition.limit = DEFAULT_PAGE_LENGTH
    if (!condition.offset) condition.offset = 0
    const list = await this.findAll(condition)
    if (_.isEmpty(list)) return onEnd()
    onData(list, condition)
    condition.offset = condition.offset + condition.limit
    await this.stream(condition, onData, onEnd)
  }

  cursor(condition) {
    return new Cursor(this.model, condition)
  }

  buildFilter(filters) {
    const where = {}
    _.forEach(filters, (filter) => {
      if (filter.lower) {
        where[filter.name] = dbConnection.where(dbConnection.fn('LOWER', dbConnection.col(filter.name)), 'LIKE', '%' + filter.value + '%')
      }
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }
      where[filter.name] = { [filter.method]: filter.value }
    })

    return where
  }

  buildFilterOr(filters) {
    const where = {}
    _.forEach(filters, (filter) => {
      if (filter.lower) {
        where[filter.name] = dbConnection.where(dbConnection.fn('LOWER', dbConnection.col(filter.name)), 'LIKE', '%' + filter.value + '%')
      }
      if (filter.valueType === 'col') {
        where[filter.name] = { [filter.method]: dbConnection.col(filter.value) }
        return
      }
      where[filter.name] = { [filter.method]: filter.value }
    })

    return {[QUERY_METHOD_MAPPING.OR] : where}
  }

  buildIncludeFilter() {

  }

  async search(conditions) {
    const limit = _.get(conditions, 'limit', DEFAULT_PAGE_LENGTH)
    const currentPage = _.get(conditions, 'page', 1)
    let options = {
      offset: (currentPage - 1) * limit,
      limit,
    }
    if (conditions.attributes) options.attributes = conditions.attributes

    if (conditions.filters) {
      options.where = this.buildFilter(conditions.filters)
    }
    if (conditions.filtersOr) {
      options.where = {...options.where, ...this.buildFilterOr(conditions.filtersOr)}
    }
    const include = this.buildIncludeFilter(conditions)
    if (!_.isEmpty(include)) options.include = include
    if (conditions.group) options.group = conditions.group

    if (conditions.orders) {
      const order = []
      _.forEach(conditions.orders, (item) => {
        order.push([item.field, item.asc ? 'ASC' : 'DESC'])
      })
      options.order = order
    }
    if (conditions.subQuery !== undefined) options.subQuery = conditions.subQuery

    const items = await this.findAndCountAll(options)

    return {
      totalItems: items.count,
      items: items.rows,
      currentPage,
      totalPages: _.ceil(items.count / limit),
    }
  }
}

module.exports = Base



