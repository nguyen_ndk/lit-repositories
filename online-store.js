const {OnlineStore} = require('lit-models')
const Base = require('./base')

class OnlineStoreRepo extends Base {
  constructor(props) {
    super(props)
  }

  findByMerchantId(merchantId) {
    return this.model.findOne({where: {merchantId}})
  }
}

module.exports = new OnlineStoreRepo(OnlineStore)