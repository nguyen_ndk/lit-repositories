const {PromotionHasCompany} = require('lit-models')
const Base = require('./base')
const _ = require('lodash')

class PromotionCompanyRepo extends Base {
  constructor(props) {
    super(props)
  }

  /*
  Create Promotion Company from list of CompaniesList
   */
  createByCompaniesIdList(promotionId, companiesList)
  {
    var promotionCompanyArray = []
    _.forEach(companiesList, companyId => {
      var data = {
        promotionId: promotionId,
        companyId: companyId
      }
      promotionCompanyArray.push(this.create(data))
    })


    return promotionCompanyArray
  }

}


module.exports = new PromotionCompanyRepo(PromotionHasCompany)