const Base = require('./base')
const {PosDevice} = require('lit-models')

class PosDeviceRepo extends Base {
  constructor(props) {
    super(props)
  }

  findByDescription(description) {
    return this.model.findOne({where: {description}})
  }
}

module.exports = new PosDeviceRepo(PosDevice)