const { PosNotificationChannel } = require('lit-models')
const Base = require('./base')

class PosNotificationChannelRepo extends Base {
  findByIdentifier(identifier) {
    return this.model.findOne({where: {identifier}})
  }
}

module.exports = new PosNotificationChannelRepo(PosNotificationChannel)
