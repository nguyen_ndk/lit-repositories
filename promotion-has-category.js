const {PromotionHasCategory} = require('lit-models')
const Base = require('./base')
const _ = require('lodash')

class PromotionCategoryRepo extends Base {
  constructor(props) {
    super(props)
  }

  /*
  Create Promotion Category from list of CompaniesList
   */
  createByCategoryIdList(promotionId, categoriesList)
  {
    var promotionCategoryArray = []
    _.forEach(categoriesList, categoryId => {
      var data = {
        promotionId: promotionId,
        categoryId: categoryId
      }
      promotionCategoryArray.push(this.create(data))
    })

    return promotionCategoryArray
  }

}

module.exports = new PromotionCategoryRepo(PromotionHasCategory)