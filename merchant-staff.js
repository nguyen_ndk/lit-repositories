// TODO remove
const {MerchantStaff, Staff} = require('lit-models')
const Base = require('./base')

class MerchantStaffRepo extends Base {
  constructor(props) {
    super(props)
  }

  findByMerchantId(options = {}){
    if (options.includeStaff) {
      options.include = [{
        model: Staff
      }]
    }

    return MerchantStaff.findAndCountAll(options)
  }

  findOneWithStaffAndMerchant(merchantId , staffId) {
    const condition = {
      where: {merchantId, staffId},
    }
    return this.model.findOne(condition)
  }
}

module.exports = new MerchantStaffRepo(MerchantStaff)