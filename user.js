const _ = require('lodash')
const moment = require('moment')
const httpError = require('http-errors')
const {QueryTypes, Op} = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {User, UserCompletion, KycResult, PaymentMethod, UserNotificationChannel, Session} = require('lit-models')
const {PAYMENT_METHOD, errorTags, QUERY_METHOD_MAPPING, USER} = require('lit-constants')
const Base = require('./base')

class UserRepo extends Base {
  constructor(model, publicData) {
    super(model, publicData)
  }

  findByEmail(email, option = {}) {
    const condition = {
      where: {email},
      include: []
    }
    if (option.includeCompletion) {
      condition.include.push({
        model: UserCompletion
      })
    }

    if (option.includeSession) {
      condition.include.push({
        model: Session,
      })
    }

    if (option.includeKyc) {
      condition.include.push({
        model: KycResult
      })
    }
    return this.model.findOne(condition)
  }

  async selectForUpdate(id, transaction) {
    const res = await DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection().query(
      'SELECT *, finance_id financeId FROM users WHERE id = $id LIMIT 1 FOR UPDATE',
      {
        bind: {id},
        type: QueryTypes.SELECT,
        transaction
      }
    )
    return res[0]
  }

  findWithActivePaymentMethod(id) {
    return this.model.findByPk(id, {
      include: {
        model: PaymentMethod,
        where: {status: PAYMENT_METHOD.STATUS.PRIMARY},
        required: false
      }
    })
  }

  async create(data) {
    const saveData = _.pick(data, ['firstName', 'lastName', 'email', 'password', 'status', 'newPhone', 'phone'])
    if (!saveData.status) saveData.status = USER.STATUS.NEW
    return this.model.create(saveData)
  }

  activateUser(id) {
    return this.update(id, {status: USER.STATUS.ACTIVE})
  }
  // TODO replace with one in validationService
  checkCreditSufficiency(u, amount) {
    if (u.credit < amount) throw httpError(422, errorTags.INSUFFICIENT_CREDIT)
  }
  // TODO replace with one in validationService
  verifyUserValidity(user) {
    if (user.status !== USER.STATUS.ACTIVE) throw httpError(403, errorTags.NOT_ALLOW)
  }

  cursorNotSyncToAccounting() {
    const condition = {
      limit: 50,
      attributes: ['id', 'status', 'email', 'phone', 'firstName', 'lastName'],
      where: {
        financeId: {
          [QUERY_METHOD_MAPPING.IS]: null
        },
        status: {
          [QUERY_METHOD_MAPPING.EQ]: USER.STATUS.ACTIVE
        }
      },
      order: [['id', 'ASC']],
    }
    return this.cursor(condition)
  }

  countNotSyncToAccounting() {
    const condition = {
      where: {
        financeId: {
          [QUERY_METHOD_MAPPING.IS]: null
        },
        status: {
          [QUERY_METHOD_MAPPING.EQ]: USER.STATUS.ACTIVE
        }
      },
    }
    return this.model.count(condition)
  }

  searchNotSyncToAccounting(page, limit) {
    const condition = {
      page,
      limit,
      attributes: ['firstName', 'lastName', 'email', 'phone', 'createdAt'],
      filters: [{
        name: 'financeId',
        method: QUERY_METHOD_MAPPING.IS,
        value: null
      },{
        name: 'status',
        method: QUERY_METHOD_MAPPING.EQ,
        value: USER.STATUS.ACTIVE
      }]
    }
    return this.search(condition)
  }

  updateFinanceId(id, financeId) {
    return this.update(id, {financeId}, {getModel: false})
  }

  getDataForMerchant(instance) {
    return _.pick(instance, USER.FIELDS_FOR_MERCHANT_VIEW)
  }

  updateCredit(userId, credit, transaction) {
    return this.update(userId, {credit}, {transaction, getModel: false})
  }

  _buildUserCompletionCompletionFilter(conditions) {
    const result = {
      model: UserCompletion
    }
    if (conditions.attributes) result.attributes = conditions.attributes
    if (!_.isEmpty(conditions.filters)) result.where = this.buildFilter(conditions.filters)
    return result
  }


  buildIncludeFilter(conditions) {
    const include = []
    if (conditions.userCompletion) {
      include.push(this._buildUserCompletionCompletionFilter(conditions.userCompletion))
    }
    return include
  }

  getGen(birthday) {
    const year = moment(birthday).format('Y')
    if (year >= 1965 && year <= 1980) return USER.GEN.GEN_X
    if (year >= 1981 && year <= 1996) return USER.GEN.MILLENNIAL
    if (year >= 1997 && year <= 2015) return USER.GEN.GEN_Z
    return USER.GEN.OTHER
  }

  findByIdWithNotification(id) {
    const condition = {where: {id}, include: [{model: UserNotificationChannel}]}
    return this.model.findOne(condition)
  }

  async checkEmailUsed(email, userId) {
    const condition = {where: {[Op.or]: {email, newEmail: email}}}
    if (userId) condition.where.id = {[Op.ne]: userId}
    const count = await this.model.count(condition)
    return count > 0
  }

  async checkPhoneUsed(phone, userId) {
    const condition = {where: {[Op.or]: {phone, newPhone: phone}}}
    if (userId) condition.where.id = {[Op.ne]: userId}
    const count = await this.model.count(condition)
    return count > 0
  }
}

module.exports = new UserRepo(User, USER.PUBLIC_DATA)