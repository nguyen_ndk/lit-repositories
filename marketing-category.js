const {MarketingCategory, Marketing, Merchant, MarketingHasCategory} = require('lit-models')
const Base = require('./base')
const {Op} = require('sequelize')

const {MARKETING_CATEGORY, MERCHANT, MARKETING, QUERY_METHOD_MAPPING} = require('lit-constants')
class MarketingCategoryRepo extends Base {
  constructor(model, publicFields) {
    super(model, publicFields)
  }
  findAllWithMerchantAndCategory() {
    const conditions = {
      where: {},
      attributes: ['name', 'slug', 'listMarketing', 'id'],
      include: [{
        model: Marketing,
        where: {
          merchantType: {[Op.in]: [MARKETING.MERCHANT_TYPE.ONLINE, MARKETING.MERCHANT_TYPE.OFFLINE_AND_ONLINE]},
          status: MARKETING.STATUS.ENABLED
        },
        attributes: ['merchantId', 'banner', 'merchantType', 'utm', 'webLink', 'appstoreLink', 'playstoreLink', 'appgalleryLink', 'id'],
        include: {
          model: Merchant,
          attributes: ['name', 'code', 'type', 'thumbnail'],
          where: {
            status: MERCHANT.STATUS.ACTIVE,
            parentMarketingId: {
              [QUERY_METHOD_MAPPING.EQ]: null //Do not take merchant with parentMarketingId
            }
          }
        }
      },
      {
        model: MarketingHasCategory,
        where: {
          status: true
        }
      }
      ]
    }
    return this.model.findAll(conditions)
  }
}
module.exports = new MarketingCategoryRepo(MarketingCategory, MARKETING_CATEGORY.PUBLIC_DATA)