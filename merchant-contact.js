const {MerchantContact} = require('lit-models')
const Base = require('./base')

class MerchantContactRepo extends Base {
  constructor(props) {
    super(props)
  }
  findByMerchantId(merchantId) {
    return this.model.findAll({where: {merchantId}})
  }
  countByMerchantId(merchantId) {
    return this.model.count({where: {merchantId}})
  }
}

module.exports = new MerchantContactRepo(MerchantContact)