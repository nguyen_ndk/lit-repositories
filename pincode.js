const {Pincode} = require('lit-models')

const findById = id => Pincode.findOne({where: {id}})

const findByUserIdAndType = (userId, type, options = {}) => {
  const conditions = {
    where: {userId, type}
  }
  if (options.transaction) {
    conditions.transaction = options.transaction
  }
  return Pincode.findOne(conditions)
}

const create = (data, options = {}) => Pincode.create(data, options)

const update = async (id, data, options = {getModel: true}) => {
  const conditions = {
    where: {id}
  }
  if (options.transaction) {
    conditions.transaction = options.transaction
  }

  const result = await Pincode.update(data, conditions)
  if (result[0] > 0 && options.getModel) {
    return await findById(id)
  }
  return result[0] > 0
}

module.exports = {
  findById,
  findByUserIdAndType,
  create,
  update,
}